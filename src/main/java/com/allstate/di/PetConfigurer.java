package com.allstate.di;

import org.springframework.beans.factory.annotation.*;
import org.springframework.context.annotation.*;

//@Configuration
@Configuration
@ComponentScan("com.allstate")
public class PetConfigurer {
    // @Bean
    // public Owner owner(@Autowired Pet pet, @Autowired Pet pet2) {
    //     return new Owner(pet, pet2);
    // }
    // @Bean
    // public Pet pet() {
    //     return new Dog();
    // }

    // @Bean
    // public Pet pet2() {
    //     return new Cat();
    // }
}
