package com.allstate.dao;

import java.util.List;

import com.allstate.entities.Employee;

import com.mongodb.client.result.UpdateResult;
import com.mongodb.internal.bulk.UpdateRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.*;
import org.springframework.stereotype.Repository;

@Repository
public class EmployeeDataMongoImpl implements EmployeeData {

    @Autowired
    MongoTemplate tpl;

    @Override
    public long count() {
        Query query = new Query();
        long result = tpl.count(query, Employee.class);
        return result;
    }

    @Override
    public Employee find(int id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Employee employee = tpl.findOne(query, Employee.class);
        return employee;
    }

    @Override
    public List<Employee> findall() {
        return tpl.findAll(Employee.class); 
    }

    @Override
    public void save(Employee employee) {
        tpl.save(employee);
    }

    @Override
    public long update(Employee employee) {
         Query query = new Query();
         query.addCriteria(Criteria.where("id").is(employee.getId()));
         Update update = new Update();
         update.set("email", employee.getEmail());
         update.set("salary", employee.getSalary());
         update.set("age", employee.getAge());
         update.set("name", employee.getName());

         UpdateResult result = tpl.updateFirst(query, update, Employee.class);

         return result.getMatchedCount();
    }


}
