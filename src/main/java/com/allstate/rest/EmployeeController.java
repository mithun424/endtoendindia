package com.allstate.rest;

import com.allstate.entities.Employee;
import com.allstate.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/api")
public class EmployeeController implements IEmployeeController {
    public EmployeeController() {
    }

    @Autowired
    EmployeeService service;

    @Override
    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public String getStatus() {
        return "Employee Rest Api is running";
    }

    @Override
    @RequestMapping(value = "/total", method = RequestMethod.GET)
    public long getTotal() {
        return service.Total();
    }

    @Override
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Employee> all() {
        return  service.all();
    }

    @Override
    @RequestMapping(value="/find/{id}", method=RequestMethod.GET)
    public ResponseEntity<Employee> find(@PathVariable("id") int id) {
        Employee employee = service.findById(id);

        if (employee == null) {
            return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
        }
        else {
            return new ResponseEntity<Employee>(employee, HttpStatus.OK);
        }
    }

    @Override
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public void save(@RequestBody Employee employee) {
        service.save(employee);
    }

    @Override
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public long update(@RequestBody Employee employee) {
        return service.update(employee);
    }
}
