package com.allstate.dao;

import com.allstate.entities.Employee;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class EmployeeDaoTest {

    @Autowired
    private EmployeeData dao;

    @Test
    public void save_And_findById_Success() {
        Employee employee = new Employee(1232,"Dee", 50,"d@i.ie", 1234.00);

        dao.save(employee);

        assertEquals(employee.getId(), dao.find(1232).getId());
    }
}
