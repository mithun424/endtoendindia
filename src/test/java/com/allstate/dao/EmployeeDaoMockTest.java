package com.allstate.dao;

import com.allstate.entities.Employee;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doNothing;

public class EmployeeDaoMockTest {

    @Mock
    private EmployeeData dao;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void rowCount_success() {
        doReturn(1L).when(dao).count();
        assertEquals(1L, dao.count());
    }
    @Test
    public void save_And_findById_Success() {
        Employee employee = new Employee(1,"Dee", 50,"d@i.ie", 1234.00);
        doNothing().when(dao).save(any(Employee.class));
    }
}
